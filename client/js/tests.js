describe("Tropical Math", function() {
  it("генерирует единичный вектор", function() { // generates a unit vector
    expect( TropMath.get1Vector(5) ).toEqual( [[0], [0], [0], [0], [0]] );
  });
  
  it("перемножает две матрицы", function() { 
    expect( TropMath.multiplyMatrix(
      [[0,1,2], [5,6,7], [-2,3,5]], 
      [[1,4,3], [2,2,2], [3,3,3]]
      )
    ).toEqual( 
      [[5,5,5], [10,10,10], [8,8,8]] 
    );
    expect( TropMath.multiplyMatrix(
      [[1,5,6], [-2,-3,7], [1,0,8]], 
      [[-3,5,6], [-2,1,0], [2,3,1]]
      )
    ).toEqual( 
      [[8,9,7], [9,10,8], [10,11,9]] 
    );
  });
  
  it("Транспонирование матрицы", function() { // generates a unit vector
    expect( TropMath.transposeMatrix(
      [[3,2,1], [5,4,0], [7,6,0]] 
      
          ) 
      ).toEqual( [[3,5,7], [2,4,6], [1,0,0]] 
      );
  });
  
  it("Транспонирование вектора", function() { // generates a unit vector
    expect( TropMath.transposeMatrix([[3], [5], [7]]
      ) 
    ).toEqual( [[3,5,7]] 
      );
  });
  
  it("Умножение матрицы на вектор", function() { // generates a unit vector
    expect( TropMath.multiplyMatrix( [[3,2,1], [5,4,0], [7,6,0]], [[1],[2],[3]]
      ) 
    ).toEqual( [[4],[6],[8]] 
      );
  });
  
  it("Умножение вектора на матрицу", function() { // generates a unit vector
    expect( TropMath.multiplyMatrix( [[3,2,1]], [[4,5,6,],[0,2,1],[3,1,0]]
      ) 
    ).toEqual( [[7,8,9]] 
      );
  });
  
  it("Инверсия", function() { // generates a unit vector
    expect( TropMath.inverseMatrix( [[4,5,6,],[0,2,1],[3,1,0]]
      ) 
    ).toEqual( [[-4,0,-3],[-5,-2,-1],[-6,-1,0]] 
      );
  });
  
  it("Сложение двух матриц", function() { // generates a unit vector
    expect( TropMath.addMatrix( [[3,4,1],[2,1,0],[5,6,7]], [[3,2,1],[2,0,5],[1,1,6]] 
      ) 
    ).toEqual( [[3,4,1],[2,1,5],[5,6,7]] 
      );
  });
  
  it("Сложение двух векторов", function() { // generates a unit vector
    expect( TropMath.addMatrix( [[3,4,1]], [[3,2,1]] 
      ) 
    ).toEqual( [[3,4,1]] 
      );
  });
  
  it("1ТС", function() { // generates a unit vector
    var n = 3;
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    // T1 = TropMath.transposeMatrix(TropMath.get1Vector(n));
    expect( TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC
      )
    ).toEqual( [[4,3,2]]
      );
  });
  
  it("gh-", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var min_h = TropMath.inverseMatrix(h);
    expect( TropMath.multiplyMatrix(g, min_h
      ) 
    ).toEqual( [[-1,-1,1],[-2,-2,0],[-3,-3,-1]] 
      );
  });
  
  it("I+gh-", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    expect( TropMath.addMatrix(I, gh
      ) 
    ).toEqual( [[0,-1,1],[-2,0,0],[-3,-3,0]] 
      );
  });
  
  it("Θ=1TC(I+gh-)1, желательно создать отдельную функцию", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Teta = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    expect( Teta
      
    ).toEqual( [[5]] 
      );
  });
  
  it("Θ-11TC", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    expect( Teta11TC 
    ).toEqual( [[ -1, -2, -3 ], [ -1, -2, -3 ], [ -1, -2, -3 ]] 
      );
  });
  
  it("Умножение матрицы на число", function() { // generates a unit vector
    expect( TropMath.multiplyonDigit( [[3,2,1], [5,4,0], [7,6,0]], 5
      ) 
    ).toEqual( [[8,7,6],[10,9,5],[12,11,5]] 
      );
  });
  
  it("Конвертирование 1х1 матрицы в число", function() { // generates a unit vector
    expect( TropMath.convertMatrixtoDigit( [[3]]
      ) 
    ).toEqual( 3
      );
  });
  
  it("I+Θ-11TC", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    expect( ITeta11TC
    ).toEqual( [ [ 0, -2, -3 ], [ -1, 0, -3 ], [ -1, -2, 0 ]] 
      );
  });
  
  it("h-(I+Θ-11TC)", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
   // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
    expect( min_hITeta11TC
    ).toEqual( [[ -3, -4, -2 ]] 
      );
  });

  it("(h-(I+Θ-11TC))- здесь нужно проверить че у нас с двумя векторами", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
        min_hITeta11TC = TropMath.inverseMatrix(min_hITeta11TC);
    expect( 
      min_hITeta11TC
    ).toEqual( [[3],[4],[2]] 
      );
  });
  
  it("x=(I+Θ-11TC)*u, вместо u подставляем g и (h-(I+Θ-11TC))- ", function() { // generates a unit vector
      var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
   var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
        min_hITeta11TC = TropMath.inverseMatrix(min_hITeta11TC);
    var vX1 = TropMath.multiplyMatrix(ITeta11TC,min_hITeta11TC);
    var vX2 = TropMath.multiplyMatrix(ITeta11TC,g);
    expect( vX1
    ).toEqual( [[3],[4],[2]] 
      );
    expect( vX2
    ).toEqual( [[3],[2],[2]] 
      );
  });
  
  it("y=Cx", function() { // generates a unit vector
    var g = [[3],[2],[1]];
    var h = [[4],[4],[2]];
    var n = 3;
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    // TropMath.multiplyMatrix(g, min_h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
        min_hITeta11TC = TropMath.inverseMatrix(min_hITeta11TC);
    var vX1 = TropMath.multiplyMatrix(ITeta11TC,min_hITeta11TC);
    var vX2 = TropMath.multiplyMatrix(ITeta11TC,g);
    var vY1 = TropMath.multiplyMatrix(mC,vX1);
    var vY2 = TropMath.multiplyMatrix(mC,vX2);
    expect( vY1
    ).toEqual( [[7],[7],[6]] 
      );
    expect( vY2
    ).toEqual( [[7],[5],[4]] 
      );
  });
});