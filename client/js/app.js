(function(angular, _, TropMath) {

    // myApp
    angular.module("myApp", ['mm.foundation', 'ngRoute', 'restangular', 'angularSpinner', 'toaster'])

    .factory('tropicalMathService', function() {
        var tropicalMathService = {
            getResult: function(mC, vU, g, h) {
                var n = mC.length;
                var m = vU.length;

                // var Cnorm = 0;
                // var max;
                // var vbuffer = TropMath.get1Vector(m);
                // var vX = [], vY = [];

                // var v1 = TropMath.get1Vector(m);

                // for (var i = 0; i < n; i++) {
                //     max = v1[i] + mC[i][0];
                //     for (var j = 0; j < m; j++) {
                //         vbuffer[i] = Math.max(v1[j] + mC[j][i], max);
                //         if (max < (v1[j] + mC[j][i]))
                //             max = v1[j] + mC[j][i];
                //     } 
                //  var g = [[3],[2],[1]];
    //var h = [[4],[4],[2]];  
    var I = TropMath.create1Matrix(n);
    var min_h = TropMath.inverseMatrix(h);
    var gh=TropMath.multiplyMatrix(g, min_h);
    //var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
    var SUMIgh=TropMath.addMatrix(I, gh);
    var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
    var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
    var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
    var TetamatrixNotInversed = _.clone(Tetamatrix, true); // debug info
    Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
    var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
    if(Teta < 0){
        Teta=-Teta;//для отчета
    }
    
    var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
    var T11C = TropMath.multiplyMatrix(T11,mC);
    var Teta11TC = TropMath.multiplyonDigit(T11C,-Teta);
    var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
    var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
    var min_hITeta11TCNotInversed = _.clone(min_hITeta11TC, true); // debug info
        min_hITeta11TC = TropMath.inverseMatrix(min_hITeta11TC);
    var vX1 = TropMath.multiplyMatrix(ITeta11TC,min_hITeta11TC);
    var vX2 = TropMath.multiplyMatrix(ITeta11TC,g);
    var vY1 = TropMath.multiplyMatrix(mC,vX1);
    var vY2 = TropMath.multiplyMatrix(mC,vX2);
                    
                    
                    
                // }

                // max = vbuffer[0] + v1[0];

//                 for (var j = 0; j < m; j++) {
//                     Cnorm = Math.max(v1[j] + vbuffer[j], max);
//                 }

//                 console.log("\nBlock 4\n");

//                 //делаем ||C||-11TC
//                 var mbuffer = TropMath.createMatrix(n, m);

//                 for (var i = 0; i < n; i++) {
//                     for (var j = 0; j < m; j++) {
//                         mbuffer[i][j] = vbuffer[j] - Cnorm;
//                         //console.log(mbuffer[i][j]);
//                     }
//                     console.log("\n");
//                 }
                
//                 var C_11TC = _.clone(mbuffer, true);

//                 //складываем с единичной матрицей
//                 var mI = TropMath.createMatrix(m, n);
//                 //max = mI[0][0]+ mbuffer[0][0];

//                 console.log(" I⊕∆−1-1TC");

//                 for (var i = 0; i < n; i++) {
//                     for (var j = 0; j < m; j++) {
//                         if (mI[i][j] > mbuffer[i][j]) {
//                             mbuffer[i][j] = mI[i][j];
//                         }
//                         else {}
//                         console.log(mbuffer[i][j]);
//                     }
//                     console.log("\n");
//                 }
                
//                 var I_d_1_1TC = _.clone(mbuffer, true);

//                 //умножаем на вектор u
//                 for (var i = 0; i < n; i++) {
//                     vX[i] = mbuffer[i][0] + vU[i];
//                     for (var j = 0; j < m; j++) {
//                         if (mbuffer[i][j] + vU[j] > vX[i]) {
//                             vX[i] = mbuffer[i][j] + vU[j];
//                         }
//                     }
//                     console.log("\n");
//                 }


//                 console.log("\nBlock 5\n");

//                 for (var i = 0; i < n; i++) {
//                     vY[i] = mC[i][0] + vX[i];
//                     for (var j = 0; j < m; j++) {
//                         if (mC[i][j] + vX[j] > vY[i]) {
//                             vY[i] = mC[i][j] + vX[j];
//                         }
//                     }
//                     console.log("\n");
//                 }
//             //    var T1 = TropMath.TransposeMatrix(TropMath.get1Vector(n));
//             //    var T1C = TropMath.multiplyMatrix(T1, mC);
//   /*  var g = [[3],[2],[1]];
//     var h = [[4],[4],[2]];
//   // var n = 3;
//     var I = TropMath.create1Matrix(n);
//     var min_h = TropMath.inverseMatrix(h);
//     var gh=TropMath.multiplyMatrix(g, min_h);
//     var mC = [[4,2,-Infinity],[1,3,-1],[0,2,2]];
//     var SUMIgh=TropMath.addMatrix(I, gh);
//     var TC1=TropMath.multiplyMatrix(TropMath.transposeMatrix(TropMath.get1Vector(n)), mC);
//     var TC1SumIGh = TropMath.multiplyMatrix(TC1,SUMIgh);
//     var Tetamatrix = TropMath.multiplyMatrix(TC1SumIGh, TropMath.get1Vector(n));
//     Tetamatrix= TropMath.inverseMatrix(Tetamatrix);
//     var Teta = TropMath.convertMatrixtoDigit(Tetamatrix);
//     var T11 = TropMath.multiplyMatrix(TropMath.get1Vector(n),TropMath.inverseMatrix(TropMath.get1Vector(n)));
//     var T11C = TropMath.multiplyMatrix(T11,mC);
//     var Teta11TC = TropMath.multiplyonDigit(T11C,Teta);
//     var ITeta11TC = TropMath.addMatrix(TropMath.create1Matrix(n), Teta11TC);
//     var min_hITeta11TC = TropMath.multiplyMatrix(min_h,ITeta11TC);
//         min_hITeta11TC = TropMath.inverseMatrix(min_hITeta11TC);
//     var vX1 = TropMath.multiplyMatrix(ITeta11TC,min_hITeta11TC);
//     var vX2 = TropMath.multiplyMatrix(ITeta11TC,g);
//     var vY1 = TropMath.multiplyMatrix(mC,vX1);
//     var vY2 = TropMath.multiplyMatrix(mC,vX2);*/
    
    
    var preparePrint = function(v) {
        var result;
        if(Array.isArray(v) && v.length > 0 && Array.isArray(v[0])) { // Матрица
            result = [];
            for(var i=0; i<v.length; i++){
                result.push(JSON.stringify(v[i]));
            }
        } else {
            result = Array.isArray(v) ? JSON.stringify(v) : v;
        }
        return result;
    }
    
                return {
                    
                    /*"v1 vector": v1,
                    "1C vector": vbuffer,
                    "Cnorm": Cnorm,
                    "||C||-11TC": C_11TC,
                    "I⊕∆−1-1TC": I_d_1_1TC,
                    "X vector": vX,
                    "Y vector": vY,
                   "T1": T1,
                    "mC":mC,
                   "T1C": T1C,*/
                   
                    
                    "mC": preparePrint(mC),
                    "vU": preparePrint(vU),
                    "g": preparePrint(g),
                    "h": preparePrint(h),
                    "n": preparePrint(n),
                    "m": preparePrint(m),
                    "I": preparePrint(I),
                    "min_h": preparePrint(min_h),
                    "gh": preparePrint(gh),
                    "SUMIgh": preparePrint(SUMIgh),
                    "TC1": preparePrint(TC1),
                    "TC1SumIGh": preparePrint(TC1SumIGh),
                    "TetamatrixNotInversed": preparePrint(TetamatrixNotInversed),
                    "Tetamatrix": preparePrint(Tetamatrix),
                    "Teta": preparePrint(Teta),
                    "T11": preparePrint(T11),
                    "T11C": preparePrint(T11C),
                    "Teta11TC": preparePrint(Teta11TC),
                    "ITeta11TC": preparePrint(ITeta11TC),
                    "min_hITeta11TCNotInversed": preparePrint(min_hITeta11TCNotInversed),
                    "min_hITeta11TC": preparePrint(min_hITeta11TC),
                    "_vX1": preparePrint(vX1),
                    "_vX2": preparePrint(vX2),
                    "_vY1": preparePrint(vY1),
                    "_vY2": preparePrint(vY2),
                   
                   
                    vX1: vX1,
                    vX2: vX2,
                    vY1: vY1,
                    vY2: vY2 
                   
                }
            }
        };
        return tropicalMathService;
    })
    
    .config(function(RestangularProvider, $routeProvider, $locationProvider, $httpProvider) {
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('!');
        
        RestangularProvider
        .setBaseUrl("http://tmath-a3mg.c9.io/")
        .setRestangularFields({
            id: '_id'
        })
        .setRequestInterceptor(
            function(elem, operation, what) {
                if (operation === 'put') {
                    elem._id = undefined;
                    return elem;
                }
                return elem;
            }
        );

        $routeProvider
        // route for the home page
        .when('/', {
            /*templateUrl: 'main.html',
            controller: 'mainController'*/
            templateUrl: 'projects.html',
            controller: 'projectController'
        })

        // route projects list
        .when('/projects', {
            templateUrl: 'projects.html',
            controller: 'projectController'
        })

        // route for the project page
        .when('/project/:id', {
            templateUrl: 'project.html',
            controller: 'projectTaskController'
        })

        .otherwise({
            templateUrl: '404.html'
        });
    })
    
    .run(function($rootScope, usSpinnerService) {
            $rootScope.startSpin = function(id){
                usSpinnerService.spin('spinner-'+id);
            }
            $rootScope.stopSpin = function(id){
                usSpinnerService.stop('spinner-'+id);
            }
    })

    .controller('projectController', function($scope, $route, $routeParams, $location, Restangular, toaster, $modal, $log) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
        
        var projectsApi = Restangular.all("projects");
        
        $scope.refreshProjects = function() {
            $scope.projects = projectsApi.getList().$object;
        }
        $scope.refreshProjects();
        
        $scope.newProject = function() {
            var newProject = {
                name: $scope.name
            };

            projectsApi.post(newProject).then(
                function(newProject) {
                    $scope.projects.push(newProject);
                    toaster.pop('success', "success", "project created");
                },
                function(error) {
                    toaster.pop('error', "project not created", error);
                }
            );
        }

        $scope.edit = function(project) {
            var editProject = Restangular.copy(project);
            
            var modalInstance = $modal.open({
                templateUrl: 'edit_project.html',
                controller: 'EditProjectModalCtrl',
                resolve: {
                    project: function() {
                        return editProject;
                    }
                }
            });
            modalInstance.result.then(function(editProject) {
                $scope.startSpin(project._id);
                
                editProject.save().then(
                    function(updatedProject) {
                        var index = $scope.projects.indexOf(project);
                        $scope.projects[index] = updatedProject;
                        
                        $scope.stopSpin(project._id);
                        toaster.pop('success', "success", "project updated");
                    },
                    function(error) {
                        $scope.stopSpin(project._id);
                        toaster.pop('error', "project not updated", error);
                    }
                );
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        
        $scope.delete = function(project) {
            var modalInstance = $modal.open({
                templateUrl: 'delete_project.html',
                controller: 'DeleteProjectModalCtrl',
                resolve: {
                    project: function() {
                        return project;
                    }
                }
            });
            modalInstance.result.then(function(project) {
                $scope.startSpin(project._id);
                
                project.remove().then(
                    function() {
                        var index = $scope.projects.indexOf(project);
                        $scope.projects.splice(index, 1);
                        
                        $scope.stopSpin(project._id);
                        toaster.pop('success', "success", "project deleted");
                    },
                    function(error) {
                        $scope.stopSpin(project._id);
                        toaster.pop('error', "project not deleted", error);
                    }
                );
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    })
    
    .controller('projectTaskController', function($scope, $route, $routeParams, $location, Restangular, toaster, $modal, $log, tropicalMathService) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
        
        var id = $routeParams["id"];
        var projectApi = Restangular.one("projects", id);
        var project = $scope.project = projectApi.get().$object;
        
        $scope.refreshTasks = function() {
            $scope.tasks = projectApi.getList('tasks').$object;
        }
        $scope.refreshTasks();
        
        $scope.calculate = function() {
            /*******************************************************************
             *                  MAGIC IS HERE                                  *
             ******************************************************************/
            var tasks = $scope.tasks;
            var matrix = []; // ?
            var g = [];
            var h = [];
          //  var c = [];
             var id2index = {};
            for (var i=0; i <tasks.length; i++) {
                id2index[tasks[i]._id] = i; //как то так
            }
            
            // Все задачи 
            for (var i = 0; i < tasks.length; i++) {
                var row = _.fill(Array(tasks.length), -Infinity);
                var task = tasks[i];
                //tasks[i].relatedTasks.c
                // row[???] = task.g;
                // row[???] = task.h;
                // row[???] = task.f;
                // row[???] = task.e;
                row[i] = task.time;
                //cюда походу
                for(var j = 0; j < task.relatedTasks.length; j++) {
                    var idx = parseInt( id2index[task.relatedTasks[j].task_id]);
                    
                    row[idx] = parseInt(task.relatedTasks[j].c);
                    
                }
                
                matrix.push(row);          
            
                g.push(Array());
                g[i][0] = task.g;
                
                h.push(Array());
                h[i][0] = task.h;
            }
            
            /*for (var i = 0; i< tasks.length; i++) {
            for (var j = 0; j< tasks[0].length; j++) {
                
                matrix[i][j] = tasks.relatedTasks.c;
                
         
              }
            }*/
            // Вектор из проекта
            var vectorTask = $scope.project.vectorTask;
            var vector = _.fill(Array(vectorTask.boundaries.length), -Infinity);
            
            for (var j = 0; j < vectorTask.boundaries.length; j++) {
                var b = vectorTask.boundaries[j];
                vector[j] = b == "" ? -Infinity : parseInt(b);
            }
            
            var result = tropicalMathService.getResult(matrix, vector, g, h);

            // Проставляем даты в задачи 

            for (var i = 0; i < tasks.length; i++) {
                var task = tasks[i];
                //task.dateBegin1 = result.?;
                //task.dateBegin2 = result.?;
                //task.dateEnd1 = result.?;
                //task.dateEnd2 = result.?;
                //task.dateBegin1 = new Date();
                task.dateBegin1 = result.vX1[i];
                task.dateBegin2 = result.vX2[i];
                task.dateEnd1 = result.vY1[i];
                task.dateEnd2 = result.vY2[i];
                
                /* task.dateBegin1 = task.dateBegin1.toDateString();
                task.dateBegin2 = task.dateBegin2.toDateString();
                task.dateEnd1 = task.dateEnd1.toDateString();
                task.dateEnd2 = task.dateEnd2.toDateString(); */
            }

            
            /******************************************************************/
            
            // Вывод результата во всплавающем окне
            var modalInstance = $modal.open({
                templateUrl: 'result.html',
                controller: 'ResultModalCtrl',
                resolve: {
                    result: function() {
                        var dump = _.clone(result, true);
                        delete dump.vX1;
                        delete dump.vX2;
                        delete dump.vY1;
                        delete dump.vY2;
                        
                        return dump;
                    }
                }
            });
            modalInstance.result.then(function(result) {
                $log.info('Modal dismissed at: ' + new Date());
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
            
            /******************************************************************/
        };
        
        $scope.newTask = function() {
            var newTask = {
                name: $scope.name,
                g: null,
                h: null,
                f: null,
                e: null,
                time: null,
                relatedTasks: []
            };

            projectApi.post('tasks', newTask).then(
                function(newTask) {
                    $scope.tasks.push(newTask);
                    toaster.pop('success', "success", "task created");
                },
                function(error) {
                    toaster.pop('error', "task not created", error);
                }
            );
        }

        $scope.edit = function(task, isVector) {
            var editTask = Restangular.copy(task);
            
            var modalInstance = $modal.open({
                templateUrl: isVector ? 'edit_task_boundaries.html' : 'edit_task.html',
                controller: 'EditTaskModalCtrl',
                resolve: {
                    task: function() {
                        return editTask;
                    },
                    projectTasks: function() {
                        return $scope.tasks;
                    }
                }
            });
            
            modalInstance.result.then(function(editTask) {
                if(isVector) {
                    $scope.startSpin(project._id);
                    
                    project.vectorTask.boundaries = editTask.boundaries;
                    project.vectorTask.name = editTask.name;
                    
                    project.save().then(
                        function(updatedProject) {
                            project = $scope.project = updatedProject;
                            
                            $scope.stopSpin(project._id);
                            toaster.pop('success', "success", "project updated");
                        },
                        function(error) {
                            $scope.stopSpin(project._id);
                            toaster.pop('error', "project not updated", error);
                        }
                    );
                } else {
                    $scope.startSpin(task._id);
                    
                    editTask.save().then(
                        function(updatedTask) {
                            var index = $scope.tasks.indexOf(task);
                            $scope.tasks[index] = updatedTask;
                            
                            $scope.stopSpin(task._id);
                            toaster.pop('success', "success", "task updated");
                        },
                        function(error) {
                            $scope.stopSpin(task._id);
                            toaster.pop('error', "task not updated", error);
                        }
                    );
                }
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        
        $scope.delete = function(task) {
            var modalInstance = $modal.open({
                templateUrl: 'delete_task.html',
                controller: 'DeleteTaskModalCtrl',
                resolve: {
                    task: function() {
                        return task;
                    }
                }
            });
            modalInstance.result.then(function(task) {
                $scope.startSpin(task._id);
                
                task.remove().then(
                    function() {
                        var index = $scope.tasks.indexOf(task);
                        $scope.tasks.splice(index, 1);
                        
                        $scope.stopSpin(task._id);
                        toaster.pop('success', "success", "task deleted");
                    },
                    function(error) {
                        $scope.stopSpin(task._id);
                        toaster.pop('error', "task not deleted", error);
                    }
                );
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    })
    
    .controller('EditProjectModalCtrl', function($scope, $modalInstance, project) {
        $scope.project = project;
        $scope.ok = function() {
            $modalInstance.close(project);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    })

    .controller('DeleteProjectModalCtrl', function($scope, $modalInstance, project) {
        $scope.project = project;
        $scope.ok = function() {
            $modalInstance.close(project);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    })
    
    .controller('EditTaskModalCtrl', function($scope, $modalInstance, task, projectTasks) {
        $scope.task = task;
        $scope.ok = function() {
            $modalInstance.close(task);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.addBoundary = function() {
            task.boundaries.push(null);
        };
        $scope.deleteBoundary = function() {
            task.boundaries.pop();
        };
        $scope.newRelated = {
            $task: null,
            task_id: null,
            b: null,
            c: null,
            d: null
        };
        $scope.addRelatedTask = function() {
            var newRelatedTask =_.clone($scope.newRelated);
            
            newRelatedTask.task_id = $scope.newRelated.$task._id;
            newRelatedTask.$task = undefined;
            
            task.relatedTasks.push(newRelatedTask);
            getPossibleRelations();
        }
        $scope.deleteRelatedTask = function(relatedTask) {
            var index = task.relatedTasks.indexOf(relatedTask);
            if (index>=0) {
                task.relatedTasks.splice(index, 1);
            }
            getPossibleRelations();
        }
        $scope.getRelatedTaskName = function(relatedTask) {
            var foundTask = _.find(projectTasks, function(t){ return t._id == relatedTask.task_id });
            if(foundTask) {
                return foundTask.name;
            } else {
                $scope.deleteRelatedTask(relatedTask);
            }
        };
        var getPossibleRelations = function() {
            var existingRelationIds = _.map(task.relatedTasks, function(r){ return r.task_id });
            $scope.possibleRelations = _.filter(projectTasks, function(t){ return (existingRelationIds.indexOf(t._id) == -1) && (t._id != task._id); });
        };
        getPossibleRelations();
        
        //$scope.$watchCollection(task.relatedTasks, getPossibleRelations);
    })

    .controller('DeleteTaskModalCtrl', function($scope, $modalInstance, task) {
        $scope.task = task;
        $scope.ok = function() {
            $modalInstance.close(task);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    })

    .controller('ResultModalCtrl', function($scope, $modalInstance, result) {
        $scope.result = result;
        $scope.ok = function() {
            $modalInstance.close(result);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    })

    .controller("mainController", ["$scope", "tropicalMathService",
        function($scope, tropicalMathService) {
            $scope.range = _.range;
            var result = $scope.result = "";

            var model = $scope.model = {
                m: 3,
                n: 3,
                C: [
                    [4, 0, -555],
                    [1, 3, -1],
                    [0, 2, 2]
                ],
                vU: [3, 2, 1]
            };

            var init = function() {
                initC();
                initVU();
                calculate();
            };

            var initC = function() {
                var row_diff = model.C.length - model.n;
                var abs_row_diff = Math.abs(row_diff);

                for (var i = 0; i < abs_row_diff; i++) {
                    if (row_diff < 0) { // C.length < n
                        model.C.push([]);
                    }
                    else if (row_diff > 0) { // C.length > n
                        model.C.pop();
                    }
                }

                var row_cnt = model.C.length;
                for (var j = 0; j < row_cnt; j++) {
                    var col_diff = model.C[j].length - model.m;
                    var abs_col_diff = Math.abs(col_diff);

                    for (var i = 0; i < abs_col_diff; i++) {
                        if (col_diff < 0) { // row.length < m
                            model.C[j].push(0);
                        }
                        else if (col_diff > 0) { // row.length > m
                            model.C[j].pop();
                        }
                    }

                }
            };
            var initVU = function() {
                var diff = model.vU.length - model.m;
                var abs_diff = Math.abs(diff);

                for (var i = 0; i < abs_diff; i++) {
                    if (diff < 0) { // vU.length < m
                        model.vU.push(0);
                    }
                    else if (diff > 0) { // vU.length > m
                        model.vU.pop();
                    }
                }
            };
            var calculate = function() {
                $scope.result = tropicalMathService.getResult(model.C, model.vU);
            };

            $scope.$watch('model.n', function() {
                init();
            });
            $scope.$watch('model.m', function() {
                init();
            });

            init();
        }
    ])
})(window.angular, window.lodash, window.TropMath);