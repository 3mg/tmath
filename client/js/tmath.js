
var TropMath = {
    addMatrix: function(M1, M2) {
        this.$checkMatrix(M1);
        this.$checkMatrix(M2);
        
        var m1 = M1.length, n1 = M1[0].length;
        
        if( M2[0].length != n1 ) {
            throw "matrices are not equal";
        }
        
        var result = this.createMatrix(m1, n1);
        for (var i = 0; i < m1; i++) {
            for (var j = 0; j < n1; j++) {
                if (M1[i][j] > M2[i][j]) {
                    result[i][j] = M1[i][j];
                }
                else {
                    result[i][j] = M2[i][j];
                }
            }
        }
        
        return result;
    },
    transposeMatrix: function(M1) {
        var m = M1.length, n = M1[0].length;
        var result = this.createMatrix(n, m);
        
        for (var i=0; i<n; i++) {
            for (var j=0; j<m; j++) {
                result[i][j] = M1[j][i];
            }
        }
        
        return result;
    },
    multiplyMatrix: function(M1, M2) {
        this.$checkMatrix(M1);
        this.$checkMatrix(M2);

        var m1 = M1.length, n1 = M1[0].length;
        var m2 = M2.length, n2 = M2[0].length;
        
        if(n1 != m2) {
            throw "Invalid matrices dimensions";
        }
        
        var result = this.createMatrix(m1, n2);
        
        for (var i=0; i<m1; i++) {
            for (var j=0; j<n2; j++) {
                var sums = [];
                for (var k=0; k<n1; k++) {
                    sums.push(M1[i][k] + M2[k][j]);
                }
                result[i][j] = _.max(sums);
            }
        }
        
        return result;
    },
    inverseMatrix: function(M){
        var m = M.length, n = M[0].length;
        
        for(var i=0; i<m; i++) {
            for(var j=0; j<n; j++) {
                if(M[i][j] != 0) {
                    M[i][j] =- M[i][j];
                }
            }
        }
        
        return this.transposeMatrix(M);
        
    },
        multiplyonDigit: function(M, digit){
        var m = M.length, n = M[0].length;
        
        for(var i=0; i<m; i++) {
            for(var j=0; j<n; j++) {
                
                    M[i][j] = M[i][j]+digit;
                
            }
        }
        
        return M;
        
    },
    convertMatrixtoDigit: function(M) {
        var m = M.length, n = M[0].length;
        var result;
        if (m ===1 && n===1)
            result=M[0][0];
            else
            throw "Матрицу невозможно перевести в число";
        
        return result;
    },
    create1Matrix: function(n) {
        var M = [];
        
        for (var i = 0; i < n; i++) {
            M.push(Array(n));

            for (var j = 0; j < n; j++) {
                if (i == j) {
                    M[i][j] = 0;
                }
                else {
                    M[i][j] = -Infinity; //(минус бесконечность)
                }
            }
        }
        
        return M;
    },
    createMatrix: function(m, n) {
        var M = [];
        
        for (var i = 0; i < m; i++) {
            M.push(Array(n));

            for (var j = 0; j < n; j++) {
                M[i][j] = i + j;
            }
        }
        
        return M;
    },
    get1Vector: function(m) {
        var vector1=[];
        
        for(var i=0; i < m; i++) {
            vector1.push(Array(0));
            vector1[i][0]=0;
        }
        
        return vector1;
    },
    get1Matrix: function(n) {
      
      //createMatrix
        for(var i=0; i < n; i++) {
          //  M.push(Array(n)); // may this help ?
           // vector1.push(Array(0));
            for(var j=0; j <n; j++) {
                if(i=j)
                M[i][j]= 0;
                else
            M[i][j]=-Infinity;
            }
        }
        
        return M;
    },
    
    // внутренние методы
    $maximum: function(a, b) {
        return Math.max(a, b);
    },
    $checkMatrix: function (M) {
        if (M.length<=0) {
            throw "Invalid matrix";
        }
        if (M[0].length<=0) {
            throw "Invalid matrix";
        }
    },
    $getMatrixNorm: function(M) { // для примера, он наверное тоже внешний
        this.$checkMatrix(M);
    
        var m = M[0].length;
        var norm = 0;
        var max;
        var vbuffer = this.get1Vector(m);
        var v1 = this.get1Vector(m);

        for (var i = 0; i < n; i++) {
            max = v1[i] + M[i][0];
            for (var j = 0; j < m; j++) {
                vbuffer[i] = this.$maximum(v1[j] + M[j][i], max);
                if (max < (v1[j] + mC[j][i]))
                    max = v1[j] + mC[j][i];
            }
        }

        max = vbuffer[0] + v1[0];

        for (var j = 0; j < m; j++) {
            norm = this.$maximum(v1[j] + vbuffer[j], max);
        }
        
        return norm;
    }
};