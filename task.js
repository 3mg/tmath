var Task = require('./db.js').Task;
var _ = require("lodash");

exports.index = function(req, res){
  Task.find({ 'project_id': req.params.project }, function (err, tasks) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      for(var i=0;i<tasks.length;i++){
        if(_.isUndefined(tasks[i].relatedTasks)){
          tasks[i].relatedTasks = [];
        }
      }
      
      res.send(tasks);
  })
};

exports.new = function(req, res){
  var newTask = new Task(req.body);
  newTask.project_id = req.params.project;
  newTask.save(function (err, task) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(task);
  });
};

exports.create = function(req, res){
  var newTask = new Task(req.body);
  newTask.project_id = req.params.project;
  newTask.save(function (err, task) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(task);
  });
};

exports.show = function(req, res) {
  Task.findOne({_id: req.params.task}, function (err, task) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      if(_.isUndefined(task.relatedTasks)){
        task.relatedTasks = [];
      }
      
      res.send(task);
  });
};

exports.edit = function(req, res){
  res.send('edit forum ' + req.params.forum);
};

exports.update = function(req, res){
  Task.findOne({_id: req.params.task}, function (err, task) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }

      task.name = req.body.name;
      task.boundaries = req.body.boundaries;
      task.relatedTasks = req.body.relatedTasks;
      
      task.g = req.body.g;
      task.h = req.body.h;
      task.f = req.body.f;
      task.e = req.body.e;
      task.time = req.body.time;

      task.save(function (err, task) {
          if (err) {
              console.error(err);
              res.status(500);
              res.send(err);
          }
          
          res.send(task);
      });
  });
};

exports.destroy = function(req, res){
  Task.remove({_id: req.params.task}, function (err, data) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(data ? req.params.task : 404);
  });
};