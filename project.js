var Project = require('./db.js').Project;
var _ = require("lodash");

exports.index = function(req, res){
  Project.find(function (err, projects) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(projects);
  })
};

exports.new = function(req, res){
  var newProject = new Project(req.data);
  newProject.save(function (err, project) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(project);
  });
};

exports.create = function(req, res){
  var newProject = new Project(req.body);
  newProject.save(function (err, project) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(project);
  });
};

exports.show = function(req, res) {
  Project.findOne({_id: req.params.project}, function (err, project) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      if(_.isUndefined(project.vectorTask)) {
        project.vectorTask = {
          name: "vU task",
          boundaries: []
        };
      }
      if(_.isUndefined(project.vectorTask.name)) {
        project.vectorTask.name = "vU task";
      }
      
      res.send(project);
  });
};

exports.edit = function(req, res){
  res.send('edit forum ' + req.params.forum);
};

exports.update = function(req, res){
  Project.findOne({_id: req.params.project}, function (err, project) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }

      project.name = req.body.name;    
      project.vectorTask.boundaries = req.body.vectorTask.boundaries;    
      project.vectorTask.name = req.body.vectorTask.name;    

      project.save(function (err, project) {
          if (err) {
              console.error(err);
              res.status(500);
              res.send(err);
          }
          
          res.send(project);
      });
  });
};

exports.destroy = function(req, res){
  Project.remove({_id: req.params.project}, function (err, data) {
      if (err) {
          console.error(err);
          res.status(500);
          res.send(err);
      }
      
      res.send(data ? req.params.project : 404);
  });
};