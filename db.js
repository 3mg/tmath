var mongoose = require('mongoose');
mongoose.connect('mongodb://'+ process.env.IP +'/tmath');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // yay!
});

var projectSchema = mongoose.Schema({
    name: String,
    vectorTask: {
      name: String,
      boundaries: Array
    }
});

var taskSchema = mongoose.Schema({
    project_id: String,
    name: String,
    g: Number,
    h: Number,
    f: Number,
    e: Number,
    time: Number,
    relatedTasks: Array
});

var Project = exports.Project = mongoose.model('Project', projectSchema);
var Task = exports.Task = mongoose.model('Task', taskSchema);

Project.find(function (err, projects) {
  if (err) {
      console.error(err);
      return;
  }
  
  if(projects.length == 0) {
      // initial fill
      var newProject = new Project({ name: "test 1" });
      newProject.save(function (err, project) {
          if (err) {
              console.error(err);
              return;
          }
          
          console.log(project);
      });
      
      var newProject2 = new Project({ name: "test 2" });
      newProject2.save(function (err, project) {
          if (err) {
              console.error(err);
              return;
          }
          
          console.log(project);
      });
  }
});