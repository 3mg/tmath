var 
  http = require("http")
  , path = require("path")
  , express = require('express')
  , Resource = require('express-resource')
  , app = express();
  
app.configure(function(){
  app.use(express.bodyParser());
  app.use('/', express.static('client'));
});
  
var projects = app.resource('projects', require('./project'));
var tasks = app.resource('tasks', require('./task'));

projects.add(tasks);

var server = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("App server listening at", addr.address + ":" + addr.port);
});
